import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HalamanTestComponent } from './halaman-test/halaman-test.component';

const routes: Routes = [
  { path: '', component: HalamanTestComponent },
  { path: '', redirectTo: '', pathMatch: 'full' } // redirect to `first-component`
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
