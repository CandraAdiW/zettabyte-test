import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HalamanTestService {

  constructor(private http: HttpClient) {}

  public getDataCorrectorList(param: any): Observable<any> {
    return this.http.get("./assets/corrector-list.json").pipe(
        map((res) => { return res }),
        catchError(err => {
          return err;
        })
    );
  }

  public getDataSchoolList(param: any): Observable<any> {
    return this.http.get("./assets/school-list.json").pipe(
      map((res) => { return res }),
      catchError(err => {
        return err;
      })
    );
  }

  public getDataSchoolCorrectorList(param: any): Observable<any> {
    return this.http.get("./assets/school-corrector-list.json").pipe(
      map((res) => { return res }),
      catchError(err => {
        return err;
      })
    );
  }

  public getDataSchoolTableList(param : any): Observable<any> {
    return this.http.get("./assets/school-table-list.json").pipe(
      map((res) => { return this.paginateManual(res, param) }),
      catchError(err => {
        return err;
      })
    );
  }

  public getDataStudentsTableList(param: any): Observable<any> {
    return this.http.get("./assets/students-table-list.json").pipe(
      map((res) => { return this.paginateManual(res, param) }),
      catchError(err => {
        return err;
      })
    );
  }

  paginateManual(data: any, param : any){
    let end   = param._page * param._perPage - 1;
    let start = end - (param._perPage - 1);
    return {
      row: data.map((item: any, index: any) => (index >= start && index <= end  ? item : -1) ).filter((item: number) => item !=-1),
      count: Math.ceil(data.length / param._perPage)
    };
  }

}
