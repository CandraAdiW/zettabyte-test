import { TestBed } from '@angular/core/testing';

import { HalamanTestService } from './halaman-test.service';

describe('HalamanTestService', () => {
  let service: HalamanTestService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HalamanTestService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
