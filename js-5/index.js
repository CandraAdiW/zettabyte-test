// Direction : For each value inside the array, get the next smallest prime number value greater than the input number, if input number is already prime return that input
// For example: Input: 4, Expected: 5, Input: 14, Expected: 17, Input: 2, Expected: 2
// Expected: [2, 5, 19, 23, 37, 89]
const number = [2, 4, 18, 20, 35, 84];
const answer = [];

function cekBilanganPrima(number){
    let hasil = 0;
    for (let i = 1; i <= number; i++) {
        if (number % i == 0) {
            hasil++;
        }
    }
    return hasil
}

function result(num) {
    // Your Code Here
    for (let item of num){
        if (cekBilanganPrima(item) == 2){
            //bilangan prima push
            answer.push(item);
        }else{
            //cari bilangan prima terdekat
            for(let itemLoop = item ; itemLoop <= 100; itemLoop++){
                if (cekBilanganPrima(itemLoop) == 2) {
                    answer.push(itemLoop);
                    break;
                }
            }
        }
    }
    return answer;
}

console.log(result(number));