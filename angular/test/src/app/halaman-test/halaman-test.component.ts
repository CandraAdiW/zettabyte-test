import { ChangeDetectorRef, Component, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { HalamanTestService } from '../halaman-test.service';

@Component({
  selector: 'app-halaman-test',
  templateUrl: './halaman-test.component.html',
  styleUrls: ['./halaman-test.component.scss']
})
export class HalamanTestComponent implements OnInit {
  @ViewChild('matPaginatorSchoolTable')
  matPaginatorSchoolTable!: MatPaginator;
  @ViewChild('matPaginatorStudentTable')
  matPaginatorStudentTable!: MatPaginator;

  kolomSchool : string[] = ['shcool','student','correction','diff'];
  listPageSizeSchool: number[] = [2,5, 10, 25, 100];
  pageSizeSchool : number = 10;
  dataSchool : any = {
    row : [],
    count : 0
  }

  kolomStudent: string[] = ['student','schoolOrig','schoolCorrect','crossCorrrector'];
  listPageSizeStudent: number[] = [5, 10, 25, 100];
  pageSizeStudent: number = 10;
  dataStudents : any = {
    row: [],
    count: 0
  };
  dataCorrection: any = [];
  dataSchooolCorrection: any = [];

  formStudent!: FormGroup;
  g: any;


  constructor(
    private ServiceTest : HalamanTestService,
    private cdr         : ChangeDetectorRef,
    private formBuilder : FormBuilder,
  ) { 
   
  }

  ngOnInit(): void {
    this.initFormStudents();
    this.getListDataSchools({
      _page: 1,
      _perPage: this.pageSizeSchool
    });
    this.getListDataStudents({
      _page: 1,
      _perPage: this.pageSizeStudent
    });
    this.getDataListCrossCorrection();
    this.getListDataCorrection();
  }

  initFormStudents() {
    this.formStudent = this.formBuilder.group({
      studentCorrection : this.formBuilder.array([])
    });
    return (this.g = this.formStudent.controls)
  }

  newRow(initialData: Partial<{
    studentName      : string,
    schoolOrig       : string,
    schoolCorrection : string | undefined;
    crossCorrection  : string | undefined;
  }> = {}) {
    (this.formStudent.get('studentCorrection') as FormArray).push(this.formBuilder.group({
      studentName     : [initialData.studentName || null],
      schoolOrig      : [initialData.schoolOrig || null],
      schoolCorrection: [initialData.schoolCorrection || null],
      crossCorrection : [ initialData.crossCorrection || null]
    }));
  };


  getDataListCrossCorrection(){
    this.ServiceTest.getDataCorrectorList({}).toPromise().then(res => {
      this.dataCorrection = res;
      this.cdr.detectChanges();
    });
  }

  getListDataCorrection(){
    this.ServiceTest.getDataSchoolCorrectorList({}).toPromise().then(res => {
      this.dataSchooolCorrection = res;
      this.cdr.detectChanges();
    });
  }

  getListDataStudents(param : any){
    this.ServiceTest.getDataStudentsTableList(param).toPromise().then(res => {
      this.dataStudents = res;
      this.dataStudents.row.forEach((item : any) => {
        let data = {
          studentName      : item.student_id.last_name + item.student_id.first_name,
          schoolOrig       : item.school_origin_id.short_name,
        }
        this.newRow(data);
      });
      this.cdr.detectChanges();
    })
  }

  getListDataSchools(param : any){
    this.ServiceTest.getDataSchoolTableList(param).toPromise().then(res => {
      this.dataSchool = res;
      this.cdr.detectChanges();
    });
  }

  ngAfterViewInit() {
    this.matPaginatorSchoolTable.page.subscribe(() => {
      let paramSchool = {
        _page: this.matPaginatorSchoolTable.pageIndex + 1,
        _perPage: this.matPaginatorSchoolTable.pageSize
      };
      this.getListDataSchools(paramSchool);
    });

    this.matPaginatorStudentTable.page.subscribe(() => {
      let paramStident = {
        _page: this.matPaginatorStudentTable.pageIndex + 1,
        _perPage: this.matPaginatorStudentTable.pageSize
      };
      this.getListDataStudents(paramStident);
    });
  }

}
