// Expected result :  [7, 7, 7, 6, 92, 11]
// Direction : copy arr1 last three of element without removing default length and fill the rest value with 7

const arr1 = [12, 24, 51, 6, 92, 11];

function result(arr1) {
    // Your Code Here
    let copy = arr1.map((item,index) => (index < 3 ? item : -1)).filter(item => item != -1);
    let hasil = arr1.map((item,index) => (index < 3 ? 7 : item));
    return hasil;
}

console.log(result(arr1));