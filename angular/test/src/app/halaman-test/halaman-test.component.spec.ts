import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HalamanTestComponent } from './halaman-test.component';

describe('HalamanTestComponent', () => {
  let component: HalamanTestComponent;
  let fixture: ComponentFixture<HalamanTestComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HalamanTestComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HalamanTestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
